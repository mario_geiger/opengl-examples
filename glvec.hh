#ifndef __GLVEC__
#define __GLVEC__

#include <cmath>
#include <iostream>

struct GLVec
{
	float x, y, z;
	
	// constructeurs
	GLVec();
	GLVec(float _x, float _y, float _z);

	// comparaison
	bool operator ==(const GLVec& v) const;
	bool operator !=(const GLVec& v) const;

	// addition et soustraction
	GLVec& operator +=(const GLVec& v);
	GLVec& operator -=(const GLVec& v);
	GLVec operator -() const;

	// amplification
	GLVec& operator *=(float k);
	GLVec& operator /=(float k);

	// norme
	float lengthSquared() const;
	float length() const;
	
	// normalization
	GLVec normalized() const;

	// produit scalaire
	static float dot(const GLVec& u, const GLVec& v);

	// produit vectoriel
	static GLVec cross(const GLVec& u, const GLVec& v);

	// addition et soustraction
	friend GLVec operator + (GLVec u, const GLVec& v);
	friend GLVec operator - (GLVec u, const GLVec& v);

	// amplification par un scalaire
	friend GLVec operator * (GLVec u, float k);
	friend GLVec operator * (float k, GLVec v);
	friend GLVec operator / (GLVec u, float k);

	// surcharge pour cout << 
	friend std::ostream& operator << (std::ostream& out, const GLVec& v);
};

// implementation inline
inline GLVec::GLVec()
    : x(0.0), y(0.0), z(0.0)
{
}

inline GLVec::GLVec(float _x, float _y, float _z)
    : x(_x), y(_y), z(_z)
{
}

inline bool GLVec::operator ==(const GLVec& v) const
{
    return x == v.x && y == v.y && z == v.z;
}

inline bool GLVec::operator !=(const GLVec& v) const
{
    return !(*this == v);
}

inline GLVec& GLVec::operator +=(const GLVec& v)
{
    x += v.x; y += v.y; z += v.z;
    return *this;
}

inline GLVec& GLVec::operator -=(const GLVec& v)
{
    x -= v.x; y -= v.y; z -= v.z;
    return *this;
}

inline GLVec GLVec::operator -() const
{
    return GLVec(-x, -y, -z);
}

inline GLVec& GLVec::operator *=(float k)
{
	x *= k; y *= k; z *= k;
	return *this;
}

inline GLVec& GLVec::operator /=(float k)
{
	x /= k; y /= k; z /= k;
	return *this;
}

inline float GLVec::lengthSquared() const
{
    return x * x + y * y + z * z;
}

inline float GLVec::length() const
{
    return std::sqrt(lengthSquared());
}

inline GLVec GLVec::normalized() const
{
	float l = length();
	if (l == 0.0)
		return GLVec(0.0, 0.0, 0.0);
	else
		return *this / l;
}

inline float GLVec::dot(const GLVec& u, const GLVec& v)
{
	return u.x * v.x + u.y * v.y + u.z * v.z;
}

inline GLVec GLVec::cross(const GLVec& u, const GLVec& v)
{
	return GLVec(u.y * v.z - v.y * u.z
	           , v.x * u.z - u.x * v.z
	           , u.x * v.y - v.x * u.y);
}

inline GLVec operator +(GLVec u, const GLVec& v)
{
	return u += v;
}

inline GLVec operator -(GLVec u, const GLVec& v)
{
	return u -= v;
}

inline GLVec operator *(GLVec u, float k)
{
	return u *= k;
}

inline GLVec operator *(float k, GLVec v)
{
	return v *= k;
}

inline GLVec operator /(GLVec u, float k)
{
	return u /= k;
}

inline std::ostream& operator <<(std::ostream& out, const GLVec& v)
{
    return out << '(' << v.x << ", " << v.y << ", " << v.z << ')';
}

#endif

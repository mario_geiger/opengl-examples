#include <GL/glew.h> // pour les extentions
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <iostream>

#include "glmatrix.hh"
#include "glshader.hh"

using namespace std;

class GLView : public wxGLCanvas
{
public:
	GLView(wxFrame* parent, int* args);
	~GLView();

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);

	void keyPressed(wxKeyEvent& event);
	//void mousePress(wxMouseEvent& event);
	//void mouseMoved(wxMouseEvent& event);
	void timer1Elapsed(wxTimerEvent& event);

private:

	static int TIMER1_ID;
	wxTimer* m_timer1;

	wxGLContext *m_context;
	
	GLShader m_prog;
	GLMatrix m_viewMatrix;

DECLARE_EVENT_TABLE()
};

int GLView::TIMER1_ID(12);

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
	EVT_SIZE(GLView::resized)
	EVT_KEY_DOWN(GLView::keyPressed)
	//EVT_MOTION(GLView::mouseMoved)
	//EVT_LEFT_DOWN(GLView::mousePress)
	EVT_PAINT(GLView::render)
	EVT_TIMER(TIMER1_ID, GLView::timer1Elapsed)
END_EVENT_TABLE()

GLView::GLView(wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_timer1(new wxTimer(this, TIMER1_ID))
	, m_context(nullptr)
{
}

GLView::~GLView()
{
	if (m_context)
		delete m_context;
}

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(130,
in vec3 vertex;
in vec3 color;
uniform mat4 proj;
uniform mat4 view;
out vec3 c;
void main(void)
{
	c = color; // envoie plus ploin
	vec4 v = vec4(vertex, 1);

	gl_Position = proj * view * v;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(130,
in vec3 c;
out vec4 c_out;
void main (void)
{
	c_out = vec4(c, 1);
//	c_out = vec4(1, 0, 0, 1);
}
); 

#define VERTEX 0
#define COLOR 1

void GLView::initGL()
{
	cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	
	glewInit(); // glew charge les extensions disponibles
	
	//cout << "opengl : " << glGetString(GL_VERSION) << endl;
	//cout << "glsl : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	//cout << "exts : " << glGetString(GL_EXTENSIONS) << endl;

	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.bindAttributeLocation("vertex", VERTEX);
	m_prog.bindAttributeLocation("color", COLOR);
	m_prog.link();

	glClearColor(0.0, 0.0, 0.0, 1.0);
	//glHint(GL_FRAGMENT_SHADER_DERIVATIVE_HINT, GL_NICEST);

	m_viewMatrix = GLMatrix::translate(0, 0, -2);

	wxSizeEvent evt;
	resized(evt);
	m_timer1->Start(40);
	cout << "ok" << endl;
}
	
void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);
		
		m_prog.bind(); // OpenGL 3
		m_prog.setUniformMatrix("proj", GLMatrix::perspective(75.0, float(w) / float(h?h:1), 0.01, 1000));
		m_prog.release();
	}
	event.Skip();
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;
	//cout << "drawing" << endl;
	SetCurrent(*m_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_prog.bind(); // active le GLShader
	m_prog.setUniformMatrix("view", m_viewMatrix); // Copie la matrice dans le shader (OpenGL 3!)
	
	glEnable(GL_DEPTH_TEST); // utile meme en 2d si on veut superposer des trucs

	glPolygonMode(GL_BACK, GL_LINE);
	glEnable(GL_CULL_FACE);

	// CYAN side - FRONT
	glBegin(GL_POLYGON);
	glVertexAttrib3f(COLOR,   0.0,  1.0, 1.0 );
	glVertexAttrib3f(VERTEX,  0.5, -0.5, -0.5 );
	glVertexAttrib3f(VERTEX, -0.5, -0.5, -0.5 );
	glVertexAttrib3f(VERTEX, -0.5,  0.5, -0.5 );
	glVertexAttrib3f(VERTEX,  0.5,  0.5, -0.5 );
	glEnd();

	// White side - BACK
	glBegin(GL_POLYGON);
	glVertexAttrib3f(COLOR,   1.0,  1.0, 1.0 );
	glVertexAttrib3f(VERTEX,  0.5, -0.5, 0.5 );
	glVertexAttrib3f(VERTEX,  0.5,  0.5, 0.5 );
	glVertexAttrib3f(VERTEX, -0.5,  0.5, 0.5 );
	glVertexAttrib3f(VERTEX, -0.5, -0.5, 0.5 );
	glEnd();
	 
	// Purple side - RIGHT
	glBegin(GL_POLYGON);
	glVertexAttrib3f(COLOR,  1.0,  0.0,  1.0 );
	glVertexAttrib3f(VERTEX, 0.5, -0.5, -0.5 );
	glVertexAttrib3f(VERTEX, 0.5,  0.5, -0.5 );
	glVertexAttrib3f(VERTEX, 0.5,  0.5,  0.5 );
	glVertexAttrib3f(VERTEX, 0.5, -0.5,  0.5 );
	glEnd();
	 
	// Green side - LEFT
	glBegin(GL_POLYGON);
	glVertexAttrib3f(COLOR,   0.0,  1.0,  0.0 );
	glVertexAttrib3f(VERTEX, -0.5, -0.5,  0.5 );
	glVertexAttrib3f(VERTEX, -0.5,  0.5,  0.5 );
	glVertexAttrib3f(VERTEX, -0.5,  0.5, -0.5 );
	glVertexAttrib3f(VERTEX, -0.5, -0.5, -0.5 );
	glEnd();
	 
	// Blue side - TOP
	glBegin(GL_POLYGON);
	glVertexAttrib3f(COLOR,   0.0,  0.0,  1.0 );
	glVertexAttrib3f(VERTEX,  0.5,  0.5,  0.5 );
	glVertexAttrib3f(VERTEX,  0.5,  0.5, -0.5 );
	glVertexAttrib3f(VERTEX, -0.5,  0.5, -0.5 );
	glVertexAttrib3f(VERTEX, -0.5,  0.5,  0.5 );
	glEnd();
	 
	// Red side - BOTTOM
	glBegin(GL_POLYGON);
	glVertexAttrib3f(COLOR,   1.0,  0.0,  0.0 );
	glVertexAttrib3f(VERTEX,  0.5, -0.5, -0.5 );
	glVertexAttrib3f(VERTEX,  0.5, -0.5,  0.5 );
	glVertexAttrib3f(VERTEX, -0.5, -0.5,  0.5 );
	glVertexAttrib3f(VERTEX, -0.5, -0.5, -0.5 );
	glEnd();

	m_prog.release();

	glFlush();
	SwapBuffers();
}

// EVENTS
void GLView::keyPressed(wxKeyEvent& event)
{
	switch(event.GetKeyCode()) {
	case 'Q':
		// quit ?
		cout << "bye bye" << endl;
		break;
	case 'W':
		// Multiplie la matrice à droite
		// NEW_VIEW = OLD_VIEW . TRANSLATION
		m_viewMatrix *= GLMatrix::translate(0, -0.1, 0);
		Refresh(false);
		break;
	case 'S':
		m_viewMatrix *= GLMatrix::translate(0, 0.1, 0);
		Refresh(false);
		break;
	case 'A':
		m_viewMatrix *= GLMatrix::translate(0.1, 0, 0);
		Refresh(false);
		break;
	case 'D':
		m_viewMatrix *= GLMatrix::translate(-0.1, 0, 0);
		Refresh(false);
		break;
	}
	event.Skip();
}
/*
void GLView::mousePress(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	mouse_x = x;
	mouse_y = y;
	Refresh(false);
	event.Skip();
}

void GLView::mouseMoved(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	mouse_x = x;
	mouse_y = y;
	
	if (event.LeftIsDown()) {
		Refresh(false);
	}
	if (event.RightIsDown()) {
		Refresh(false);
	}
	event.Skip();
}
*/
void GLView::timer1Elapsed(wxTimerEvent& event)
{	
	float dt = 1.0 / 60.0 * event.GetInterval();
	m_viewMatrix *= GLMatrix::rotate(dt, 0, 1, 0);
	Refresh(false);
}







class MyApp: public wxApp
{
private:
	bool OnInit() override;

	wxFrame *_frame;
	GLView *_glPane;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	
	cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("main"), wxPoint(50,50), wxSize(500,500));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(_frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	_frame->Show();

	cout << "ok" << endl;

	_glPane->initGL();
	
	return true;
}

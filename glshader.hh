#ifndef __GLSHADER_HH__
#define __GLSHADER_HH__
#include <GL/glew.h>

#define GLSL(version, shader)  "#version " #version "\n" #shader

#include "glmatrix.hh"

// wrap minimaliste des shaders d'opengl
class GLShader
{
public:
	GLShader();
	GLShader(const GLShader &) = delete;
	~GLShader();
	
	void init();
	void free();
	
	bool addVertexSourceCode(const GLchar *src);
	bool addFragmentSourceCode(const GLchar *src);
	void bindAttributeLocation(const GLchar* name, GLuint index);
	bool link();

	void bind();
	void release();
	
	GLint uniformLocation(const GLchar *name) const;
	
	void setUniformValue(const GLchar* name, GLint value);
	void setUniformValue(GLint location, GLint value);
	void setUniformValue(const GLchar* name, GLfloat value);
	void setUniformValue(GLint location, GLfloat value);
	void setUniformValue(const GLchar* name, GLfloat value0, GLfloat value1);
	void setUniformValue(GLint location, GLfloat value0, GLfloat value1);
	void setUniformValue(const GLchar* name, GLfloat value0, GLfloat value1, GLfloat value2);
	void setUniformValue(GLint location, GLfloat value0, GLfloat value1, GLfloat value2);
	void setUniformMatrix3(const GLchar* name, const GLMatrix& matrix, bool transpose = false);
	void setUniformMatrix3(GLint location, const GLMatrix& matrix, bool transpose = false);
	void setUniformMatrix(const GLchar* name, const GLMatrix& matrix, bool transpose = false);
	void setUniformMatrix(GLint location, const GLMatrix& matrix, bool transpose = false);
	
private:
	GLuint _vertex;
	GLuint _fragment;
	GLuint _program;
	bool _init;
};

#endif

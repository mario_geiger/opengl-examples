#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <iostream>

#include "glmatrix.hh"
#include "glshader.hh"

using namespace std;

class GLView : public wxGLCanvas
{
public:
	GLView(wxFrame* parent, int* args);
	~GLView();

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);

	void keyPressed(wxKeyEvent& event);
private:

	wxGLContext *m_context;
	
	GLShader m_prog;
	GLMatrix m_viewMatrix;
	
DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
	EVT_SIZE(GLView::resized)
	EVT_KEY_DOWN(GLView::keyPressed)
	EVT_PAINT(GLView::render)
END_EVENT_TABLE()

GLView::GLView(wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_context(nullptr)
{
}

GLView::~GLView()
{
	if (m_context)
		delete m_context;
}

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(130,
out vec2 p;
void main(void)
{
	p = gl_Vertex.xy;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor = gl_Color;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(130,
in vec2 p;
void main (void)
{
	vec4 white = vec4(1, 1, 1, 1);
	vec4 black = vec4(0, 0, 0, 1);
	float radius = length(p);
	if (radius < 0.7)
		gl_FragColor = white;
	else
		gl_FragColor = black;
}
); 

void GLView::initGL()
{
	cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	glewInit();
	
	cout << "opengl : " << glGetString(GL_VERSION) << endl;
	cout << "glsl : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	//cout << "exts : " << glGetString(GL_EXTENSIONS) << endl;

	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.link();

	glClearColor(0.0, 0.0, 0.0, 1.0);

	wxSizeEvent evt;
	resized(evt);
	cout << "ok" << endl;
}
	
void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		//gluPerspective(75.0, float(w) / float(h?h:1), 0.01, 1000);
	}
	event.Skip();
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;
	cout << "drawing" << endl;
	SetCurrent(*m_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	m_prog.bind(); // active le GLShader
	glBegin(GL_QUADS);
	glVertex2f(-1,-1);
	glVertex2f(1,-1);
	glVertex2f(1,1);
	glVertex2f(-1,1);
	glEnd();
	m_prog.release();

	glFlush();
	SwapBuffers();
}

// EVENTS
void GLView::keyPressed(wxKeyEvent& event)
{
	switch(event.GetKeyCode()) {
	case 'Q':
		// quit ?
		cout << "bye bye" << endl;
		break;
	}
	event.Skip();
}








class MyApp: public wxApp
{
private:
	bool OnInit() override;

	wxFrame *_frame;
	GLView *_glPane;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	
	cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("clouds"), wxPoint(50,50), wxSize(500,500));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(_frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	_frame->Show();

	cout << "ok" << endl;

	_glPane->initGL();
	
	return true;
}

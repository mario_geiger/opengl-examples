#include "glshader.hh"
#include <iostream>
#include <cstring>
using namespace std;

GLShader::GLShader()
	: _vertex(0)
	, _fragment(0)
	, _program(0)
	, _init(false)
{
}

GLShader::~GLShader()
{
	free();
}

void GLShader::init()
{
	if (_init)
		cerr << "shader already initialized" << endl;
	_vertex = glCreateShader(GL_VERTEX_SHADER);
	_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	_program = glCreateProgram();
	glAttachShader(_program, _vertex);
	glAttachShader(_program, _fragment);
	_init = true;
}

void GLShader::free()
{
	if (_init) {
		glDetachShader(_program, _vertex);
		glDetachShader(_program, _fragment);
		glDeleteObjectARB(_vertex);
		glDeleteObjectARB(_fragment);
		glDeleteShader(_program);
	}
}

bool GLShader::addVertexSourceCode(const GLchar *src)
{
	if (!_init) {
		cerr << "shader not initialized" << endl;
		return false;
	}
	
	GLint len = strlen(src);
	glShaderSourceARB(_vertex, 1, &src, &len);
	glCompileShaderARB(_vertex);
	
	GLint compiled = 0;
	glGetShaderiv(_vertex, GL_COMPILE_STATUS, &compiled);

	if (compiled == GL_TRUE)
		return true;

	GLint blen = 0;
	GLsizei slen = 0;

	glGetShaderiv(_vertex, GL_INFO_LOG_LENGTH , &blen);       
	if (blen > 1)
	{
		GLchar* compiler_log = new GLchar[blen];
		glGetInfoLogARB(_vertex, blen, &slen, compiler_log);
		cout << "vertex_compiler_log:\n" << compiler_log << endl;
		delete[] compiler_log;
	}
	return false;
}

bool GLShader::addFragmentSourceCode(const GLchar *src)
{
	if (!_init) {
		cerr << "shader not initialized" << endl;
		return false;
	}

	GLint len = strlen(src);
	glShaderSourceARB(_fragment, 1, &src, &len);
	glCompileShaderARB(_fragment);
	
	GLint compiled = 0;
	glGetShaderiv(_fragment, GL_COMPILE_STATUS, &compiled);

	if (compiled == GL_TRUE)
		return true;

	GLint blen = 0;
	GLsizei slen = 0;

	glGetShaderiv(_fragment, GL_INFO_LOG_LENGTH , &blen);       
	if (blen > 1)
	{
		GLchar* compiler_log = new GLchar[blen];
		glGetInfoLogARB(_fragment, blen, &slen, compiler_log);
		cerr << "fragment_compiler_log:\n" << compiler_log << endl;
		delete[] compiler_log;
	}
	return false;
}

void GLShader::bindAttributeLocation(const GLchar* name, GLuint index)
{
	glBindAttribLocation(_program, index, name);
}

bool GLShader::link()
{
	if (!_init) {
		cerr << "shader not initialized" << endl;
		return false;
	}
	
	glLinkProgram(_program);

	GLint linked;
	glGetProgramiv(_program, GL_LINK_STATUS, &linked);
	
	if (linked == GL_FALSE)
		cerr << "program_link_error" << endl;

	return linked == GL_TRUE;
}

void GLShader::bind()
{
	glUseProgram(_program);
}

void GLShader::release()
{
	glUseProgram(0);
}

GLint GLShader::uniformLocation(const GLchar *name) const
{
	return glGetUniformLocation(_program, name);
}

void GLShader::setUniformValue(const GLchar *name, GLint value)
{
	setUniformValue(uniformLocation(name), value);
}

void GLShader::setUniformValue(GLint location, GLint value)
{
	glUniform1i(location, value);
}

void GLShader::setUniformValue(const GLchar *name, GLfloat value)
{
	setUniformValue(uniformLocation(name), value);
}

void GLShader::setUniformValue(GLint location, GLfloat value)
{
	glUniform1f(location, value);
}

void GLShader::setUniformValue(const GLchar* name, GLfloat value0, GLfloat value1)
{
	setUniformValue(uniformLocation(name), value0, value1);
}

void GLShader::setUniformValue(GLint location, GLfloat value0, GLfloat value1)
{
	glUniform2f(location, value0, value1);
}

void GLShader::setUniformValue(const GLchar *name, GLfloat value0, GLfloat value1, GLfloat value2)
{
	setUniformValue(uniformLocation(name), value0, value1, value2);
}

void GLShader::setUniformValue(GLint location, GLfloat value0, GLfloat value1, GLfloat value2)
{
	glUniform3f(location, value0, value1, value2);
}

void GLShader::setUniformMatrix3(const GLchar* name, const GLMatrix& matrix, bool transpose)
{
	setUniformMatrix3(uniformLocation(name), matrix, transpose);
}

void GLShader::setUniformMatrix3(GLint location, const GLMatrix& matrix, bool transpose)
{
	float v[3][3];
	v[0][0] = matrix(0,0);v[0][1] = matrix(1,0);v[0][2] = matrix(2,0);
	v[1][0] = matrix(0,1);v[1][1] = matrix(1,1);v[1][2] = matrix(2,1);
	v[2][0] = matrix(0,2);v[2][1] = matrix(1,2);v[2][2] = matrix(2,2);
	glUniformMatrix3fv(location, 1, transpose, &v[0][0]);
}

void GLShader::setUniformMatrix(const GLchar* name, const GLMatrix& matrix, bool transpose)
{
	setUniformMatrix(uniformLocation(name), matrix, transpose);
}

void GLShader::setUniformMatrix(GLint location, const GLMatrix& matrix, bool transpose)
{
	glUniformMatrix4fv(location, 1, transpose, matrix.data());
}

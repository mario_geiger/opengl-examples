#include "glmatrix.hh"
#include <cmath>

GLMatrix GLMatrix::scale(float x, float y, float z)
{
	GLMatrix sca;
	sca.m[0][0] = x;
	sca.m[1][1] = y;
	sca.m[2][2] = z;
	return sca;
}

GLMatrix GLMatrix::translate(float x, float y, float z)
{
	 GLMatrix tra;
	 tra.m[3][0] = x;
	 tra.m[3][1] = y;
	 tra.m[3][2] = z;
	 return tra;
}

GLMatrix GLMatrix::rotate(float angle, float x, float y, float z)
{
	// http://fr.wikipedia.org/wiki/Matrice_de_rotation#Matrices_de_rotation_dans_le_cas_g.C3.A9n.C3.A9ral
	float a = angle * M_PI / 180.0;
	float c = cosf(a);
	float s = sinf(a);
	double len = double(x) * double(x) +
	             double(y) * double(y) +
	             double(z) * double(z);
    if (len != 1.0 && len != 0.0) {
		len = sqrt(len);
		x = float(double(x) / len);
		y = float(double(y) / len);
		z = float(double(z) / len);
	}
	float ic = 1.0f - c;
	GLMatrix rot;
	rot.m[0][0] = x * x * ic + c;
	rot.m[1][0] = x * y * ic - z * s;
	rot.m[2][0] = x * z * ic + y * s;

	rot.m[0][1] = y * x * ic + z * s;
	rot.m[1][1] = y * y * ic + c;
	rot.m[2][1] = y * z * ic - x * s;

	rot.m[0][2] = x * z * ic - y * s;
	rot.m[1][2] = y * z * ic + x * s;
	rot.m[2][2] = z * z * ic + c;

	return rot;
}

GLMatrix GLMatrix::perspective(float angle, float aspect, float near, float far)
{
	float f = 1.0 / std::tan(M_PI * angle / (2.0 * 180.0));
	GLMatrix per;
	// 1st column
	per.m[0][0] = f / aspect;
	
	// 2nd column
	per.m[1][1] = f;
	
	// 3rd column
	per.m[2][2] = (far + near) / (near - far);
	per.m[2][3] = -1;
	
	// 4th column
	per.m[3][2] = (2.0 * far * near) / (near - far);
	per.m[3][3] = 0.0;
	
	return per;
}

GLMatrix GLMatrix::ortho(float left, float right, float bottom, float top, float near, float far)
{
	GLMatrix ort;
	// 1st column
	ort.m[0][0] = 2.0 / (right - left);
	
	// 2nd column
	ort.m[1][1] = 2.0 / (top - bottom);
	
	// 3rd column
	ort.m[2][2] = -2.0 / (far - near);
	
	// 4th column
	ort.m[3][0] = - (right + left) / (right - left);
	ort.m[3][1] = - (top + bottom) / (top - bottom);
	ort.m[3][2] = - (far + near) / (far - near);
	
	return ort;
}


GLMatrix &GLMatrix::operator *=(const GLMatrix &other)
{
	float m0, m1, m2;
	m0 = m[0][0] * other.m[0][0]
	   + m[1][0] * other.m[0][1]
	   + m[2][0] * other.m[0][2]
	   + m[3][0] * other.m[0][3];
	m1 = m[0][0] * other.m[1][0]
	   + m[1][0] * other.m[1][1]
	   + m[2][0] * other.m[1][2]
	   + m[3][0] * other.m[1][3];
	m2 = m[0][0] * other.m[2][0]
	   + m[1][0] * other.m[2][1]
	   + m[2][0] * other.m[2][2]
	   + m[3][0] * other.m[2][3];
	m[3][0] = m[0][0] * other.m[3][0]
	        + m[1][0] * other.m[3][1]
	        + m[2][0] * other.m[3][2]
	        + m[3][0] * other.m[3][3];
	m[0][0] = m0;
	m[1][0] = m1;
	m[2][0] = m2;

	m0 = m[0][1] * other.m[0][0]
	   + m[1][1] * other.m[0][1]
	   + m[2][1] * other.m[0][2]
	   + m[3][1] * other.m[0][3];
	m1 = m[0][1] * other.m[1][0]
	   + m[1][1] * other.m[1][1]
	   + m[2][1] * other.m[1][2]
	   + m[3][1] * other.m[1][3];
	m2 = m[0][1] * other.m[2][0]
	   + m[1][1] * other.m[2][1]
	   + m[2][1] * other.m[2][2]
	   + m[3][1] * other.m[2][3];
	m[3][1] = m[0][1] * other.m[3][0]
	   + m[1][1] * other.m[3][1]
	   + m[2][1] * other.m[3][2]
	   + m[3][1] * other.m[3][3];
	m[0][1] = m0;
	m[1][1] = m1;
	m[2][1] = m2;

	m0 = m[0][2] * other.m[0][0]
	   + m[1][2] * other.m[0][1]
	   + m[2][2] * other.m[0][2]
	   + m[3][2] * other.m[0][3];
	m1 = m[0][2] * other.m[1][0]
	   + m[1][2] * other.m[1][1]
	   + m[2][2] * other.m[1][2]
	   + m[3][2] * other.m[1][3];
	m2 = m[0][2] * other.m[2][0]
	   + m[1][2] * other.m[2][1]
	   + m[2][2] * other.m[2][2]
	   + m[3][2] * other.m[2][3];
	m[3][2] = m[0][2] * other.m[3][0]
	        + m[1][2] * other.m[3][1]
	        + m[2][2] * other.m[3][2]
	        + m[3][2] * other.m[3][3];
	m[0][2] = m0;
	m[1][2] = m1;
	m[2][2] = m2;

	m0 = m[0][3] * other.m[0][0]
	   + m[1][3] * other.m[0][1]
	   + m[2][3] * other.m[0][2]
	   + m[3][3] * other.m[0][3];
	m1 = m[0][3] * other.m[1][0]
	   + m[1][3] * other.m[1][1]
	   + m[2][3] * other.m[1][2]
	   + m[3][3] * other.m[1][3];
	m2 = m[0][3] * other.m[2][0]
	   + m[1][3] * other.m[2][1]
	   + m[2][3] * other.m[2][2]
	   + m[3][3] * other.m[2][3];
	m[3][3] = m[0][3] * other.m[3][0]
	        + m[1][3] * other.m[3][1]
	        + m[2][3] * other.m[3][2]
	        + m[3][3] * other.m[3][3];
	m[0][3] = m0;
	m[1][3] = m1;
	m[2][3] = m2;
	return *this;
}

static inline float matrixDet3
    (const float m[4][4], int col0, int col1, int col2,
     int row0, int row1, int row2)
{
	return m[col0][row0] *
				(m[col1][row1] * m[col2][row2] -
				 m[col1][row2] * m[col2][row1]) -
		   m[col1][row0] *
				(m[col0][row1] * m[col2][row2] -
				 m[col0][row2] * m[col2][row1]) +
		   m[col2][row0] *
				(m[col0][row1] * m[col1][row2] -
				 m[col0][row2] * m[col1][row1]);
}

// Calculate the determinant of a 4x4 matrix.
static inline float matrixDet4(const float m[4][4])
{
	float det;
	det  = m[0][0] * matrixDet3(m, 1, 2, 3, 1, 2, 3);
	det -= m[1][0] * matrixDet3(m, 0, 2, 3, 1, 2, 3);
	det += m[2][0] * matrixDet3(m, 0, 1, 3, 1, 2, 3);
	det -= m[3][0] * matrixDet3(m, 0, 1, 2, 1, 2, 3);
	return det;
}

GLMatrix GLMatrix::inverted(bool *invertible) const
{
	GLMatrix inv;

	float det = matrixDet4(m);
	if (det == 0.0f) {
		if (invertible)
			*invertible = false;
		return GLMatrix();
	}
	det = 1.0f / det;

	inv.m[0][0] =  matrixDet3(m, 1, 2, 3, 1, 2, 3) * det;
	inv.m[0][1] = -matrixDet3(m, 0, 2, 3, 1, 2, 3) * det;
	inv.m[0][2] =  matrixDet3(m, 0, 1, 3, 1, 2, 3) * det;
	inv.m[0][3] = -matrixDet3(m, 0, 1, 2, 1, 2, 3) * det;
	inv.m[1][0] = -matrixDet3(m, 1, 2, 3, 0, 2, 3) * det;
	inv.m[1][1] =  matrixDet3(m, 0, 2, 3, 0, 2, 3) * det;
	inv.m[1][2] = -matrixDet3(m, 0, 1, 3, 0, 2, 3) * det;
	inv.m[1][3] =  matrixDet3(m, 0, 1, 2, 0, 2, 3) * det;
	inv.m[2][0] =  matrixDet3(m, 1, 2, 3, 0, 1, 3) * det;
	inv.m[2][1] = -matrixDet3(m, 0, 2, 3, 0, 1, 3) * det;
	inv.m[2][2] =  matrixDet3(m, 0, 1, 3, 0, 1, 3) * det;
	inv.m[2][3] = -matrixDet3(m, 0, 1, 2, 0, 1, 3) * det;
	inv.m[3][0] = -matrixDet3(m, 1, 2, 3, 0, 1, 2) * det;
	inv.m[3][1] =  matrixDet3(m, 0, 2, 3, 0, 1, 2) * det;
	inv.m[3][2] = -matrixDet3(m, 0, 1, 3, 0, 1, 2) * det;
	inv.m[3][3] =  matrixDet3(m, 0, 1, 2, 0, 1, 2) * det;

	if (invertible)
		*invertible = true;
	return inv;
}

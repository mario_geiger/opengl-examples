#ifndef __GLMATRIX__
#define __GLMATRIX__

#include "glvec.hh"

class GLMatrix {
public:
	GLMatrix();
	//Matrix4 &operator =(const Matrix4& other);
	static GLMatrix scale(float x, float y, float z);
	static GLMatrix translate(float x, float y, float z);
	static GLMatrix rotate(float angle, float x, float y, float z);
	static GLMatrix perspective(float angle, float aspect, float near, float far);
	static GLMatrix ortho(float left, float right, float bottom, float top, float near, float far);
	
	float& operator ()(int i, int j);
	float operator ()(int i, int j) const;
	GLMatrix& operator *=(const GLMatrix &b);
	const float* data() const;
	GLVec operator *(const GLVec& v) const;

	GLMatrix inverted(bool* invertible = nullptr) const;

private:
	float m[4][4];
	// m[0] : 1st column
	// m[1] : 2nd column
	// ...
};

inline const GLMatrix operator *(GLMatrix a, const GLMatrix &b) {
	return a *= b;
}

inline GLMatrix::GLMatrix() 
{
	m[0][0] = 1.0; m[1][0] = 0.0; m[2][0] = 0.0; m[3][0] = 0.0;
	m[0][1] = 0.0; m[1][1] = 1.0; m[2][1] = 0.0; m[3][1] = 0.0;
	m[0][2] = 0.0; m[1][2] = 0.0; m[2][2] = 1.0; m[3][2] = 0.0;
	m[0][3] = 0.0; m[1][3] = 0.0; m[2][3] = 0.0; m[3][3] = 1.0;
}

inline float &GLMatrix::operator ()(int i, int j)
{
	return m[j][i];
}

inline float GLMatrix::operator ()(int i, int j) const
{
	return m[j][i];
}

inline const float *GLMatrix::data() const
{
	return *m;
}

inline GLVec GLMatrix::operator *(const GLVec& v) const
{
	return GLVec(m[0][0] * v.x + m[1][0] * v.y + m[2][0] * v.z + m[3][0],
	             m[0][1] * v.x + m[1][1] * v.y + m[2][1] * v.z + m[3][1],
	             m[0][2] * v.x + m[1][2] * v.y + m[2][2] * v.z + m[3][2]);
}

#endif

#include <GL/glew.h> // pour les extentions
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <iostream>

#include "glmatrix.hh"
#include "glshader.hh"
#include "glvec.hh"

using namespace std;

class GLView : public wxGLCanvas
{
public:
	GLView(wxFrame* parent, int* args);
	~GLView();

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);
	
private:
	wxGLContext *m_context;
	
	GLShader m_prog;
	GLMatrix m_modelMatrix;
	GLMatrix m_viewMatrix;
	
	DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
	EVT_SIZE(GLView::resized)
	EVT_PAINT(GLView::render)
END_EVENT_TABLE()

GLView::GLView(wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_context(nullptr)
{
}

GLView::~GLView()
{
	if (m_context)
		delete m_context;
}

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(130,
in vec2 vertex;
uniform mat4 pmat;
out vec2 p;
void main(void)
{
	p = vertex;
	gl_Position = pmat * vec4(vertex, 0, 1);
}
);

// fragment shader
static const GLchar *fsrc = GLSL(130,
in vec2 p;
uniform vec2 pos[2];
out vec4 c_out;
void main (void)
{
	float a = distance(p, pos[0]);
	float b = distance(p, pos[1]);
	float v = 1.0 / a - 1.0 / b;
	v = pow(sin(10.0 / v), 2.0);
	if (v < 0.9) discard;
	c_out = vec4(v, v, v, 1);
}
); 

#define VERTEX 0

void GLView::initGL()
{
	//cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	
	glewInit(); // glew charge les extensions disponibles
	
	//cout << "opengl : " << glGetString(GL_VERSION) << endl;
	//cout << "glsl : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	//cout << "exts : " << glGetString(GL_EXTENSIONS) << endl;

	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.bindAttributeLocation("vertex", VERTEX);
	if (!m_prog.link())
		cout << "not linked" << endl;
		
	m_prog.bind();
	float pos[] = {0.0, 0.0, 0.5, 0.0};
	glUniform2fv(m_prog.uniformLocation("pos"), 2, pos);
	m_prog.release();
		
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glHint(GL_FRAGMENT_SHADER_DERIVATIVE_HINT, GL_NICEST);

	wxSizeEvent evt;
	resized(evt);
	//cout << "ok" << endl;
}
	
void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);
		
		m_prog.bind(); // OpenGL 3
		float x = float(w) / float(h?h:1);
		float y = float(h) / float(w?w:1);
		if (x < y)
			m_prog.setUniformMatrix("pmat", GLMatrix::ortho(-1.0, 1.0, -y, y, -1, 1));
		else
			m_prog.setUniformMatrix("pmat", GLMatrix::ortho(-x, x, -1.0, 1.0, -1, 1));
		m_prog.release();
	}
	event.Skip();
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;
	//cout << "drawing" << endl;
	SetCurrent(*m_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_prog.bind(); // active le GLShader
	
	glBegin(GL_QUADS);
	glVertexAttrib2f(VERTEX, -1.0, -1.0);
	glVertexAttrib2f(VERTEX, 1.0, -1.0);
	glVertexAttrib2f(VERTEX, 1.0, 1.0);
	glVertexAttrib2f(VERTEX, -1.0, 1.0);
	glEnd();

	m_prog.release();

	glFlush();
	SwapBuffers();
}







class MyApp: public wxApp
{
private:
	bool OnInit() override;

	wxFrame *_frame;
	GLView *_glPane;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	
	//cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("main"), wxPoint(50,50), wxSize(500,500));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(_frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	_frame->Show();

	//cout << "ok" << endl;

	_glPane->initGL();
	
	return true;
}

#include <GL/glew.h> // pour les extentions
//#include <GL/glu.h> // plus besoin de ca pour la matrice de perspective avec opengl3 (on fait nous meme ^^)
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <iostream>

#include "glmatrix.hh"
#include "glshader.hh"

using namespace std;

class GLView : public wxGLCanvas
{
public:
	GLView(wxFrame* parent, int* args);
	~GLView();

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);

	void keyPressed(wxKeyEvent& event);
private:

	wxGLContext *m_context;
	
	GLShader m_prog;
	GLMatrix m_viewMatrix;
	
DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
	EVT_SIZE(GLView::resized)
	EVT_KEY_DOWN(GLView::keyPressed)
	EVT_PAINT(GLView::render)
END_EVENT_TABLE()

GLView::GLView(wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_context(nullptr)
{
}

GLView::~GLView()
{
	if (m_context)
		delete m_context;
}

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(130,
uniform mat4 proj;
uniform mat4 view;
out vec2 p;
void main(void)
{
//	p = (gl_ModelViewMatrix * gl_Vertex).xy; // vieille méthode
	p = (view * gl_Vertex).xy; // nouvelle méthode : avec la matrice passée dans une varialbe uniform
//	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = proj * view * gl_Vertex;
	gl_FrontColor = gl_Color;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(130,
in vec2 p;
void main (void)
{
	float l = length(p);
	gl_FragColor = vec4(1,1,1,1) * l;
}
); 

void GLView::initGL()
{
	cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	
	glewInit(); // glew charge les extensions disponibles
	
	//cout << "opengl : " << glGetString(GL_VERSION) << endl;
	//cout << "glsl : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	//cout << "exts : " << glGetString(GL_EXTENSIONS) << endl;

	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.link();

	glClearColor(0.0, 0.0, 0.0, 1.0);

	wxSizeEvent evt;
	resized(evt);
	cout << "ok" << endl;
}
	
void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);

		//glMatrixMode(GL_PROJECTION);    // OpenGL 1
		//glLoadIdentity();
		//gluPerspective(75.0, float(w) / float(h?h:1), 0.01, 1000);
		
		m_prog.bind(); // OpenGL 3
		m_prog.setUniformMatrix("proj", GLMatrix::perspective(75.0, float(w) / float(h?h:1), 0.01, 1000));
		m_prog.release();
	}
	event.Skip();
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;
	//cout << "drawing" << endl;
	SetCurrent(*m_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();	
	//glLoadMatrixf(m_viewMatrix.data()); // Copie la matrice dans la matrice MODELVIEW de opengl (vieille méthode)

	m_prog.bind(); // active le GLShader
	m_prog.setUniformMatrix("view", m_viewMatrix); // Copie la matrice dans le shader (OpenGL 3!)
	
	glEnable(GL_DEPTH_TEST);

	glBegin(GL_QUADS); // dessine un rectangle ("mauvaise méthode")
	glVertex3f(-1,-1, -5);
	glVertex3f(1,-1, -5);
	glVertex3f(1,1, -10);
	glVertex3f(-1,1, -10);
	glEnd();
	m_prog.release();

	glFlush();
	SwapBuffers();
}

// EVENTS
void GLView::keyPressed(wxKeyEvent& event)
{
	switch(event.GetKeyCode()) {
	case 'Q':
		// quit ?
		cout << "bye bye" << endl;
		break;
	case 'W':
		// Multiplie la matrice à droite
		// NEW_VIEW = OLD_VIEW . TRANSLATION
		m_viewMatrix *= GLMatrix::translate(0, 0, 1);
		Refresh(false);
		break;
	case 'S':
		m_viewMatrix *= GLMatrix::translate(0, 0, -1);
		Refresh(false);
		break;
	case 'A':
		m_viewMatrix *= GLMatrix::translate(1, 0, 0);
		Refresh(false);
		break;
	case 'D':
		m_viewMatrix *= GLMatrix::translate(-1, 0, 0);
		Refresh(false);
		break;
	}
	event.Skip();
}








class MyApp: public wxApp
{
private:
	bool OnInit() override;

	wxFrame *_frame;
	GLView *_glPane;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	
	cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("clouds"), wxPoint(50,50), wxSize(500,500));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(_frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	_frame->Show();

	cout << "ok" << endl;

	_glPane->initGL();
	
	return true;
}

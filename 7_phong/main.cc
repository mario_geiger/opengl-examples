#include <GL/glew.h> // pour les extentions
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <iostream>

#include "glmatrix.hh"
#include "glshader.hh"
#include "glvec.hh"

using namespace std;

class GLView : public wxGLCanvas
{
public:
	GLView(wxFrame* parent, int* args);
	~GLView();

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);

	void keyPressed(wxKeyEvent& event);
	void mousePress(wxMouseEvent& event);
	void mouseMoved(wxMouseEvent& event);
	void timer1Elapsed(wxTimerEvent& event);

private:

	static int TIMER1_ID;
	wxTimer* m_timer1;

	wxGLContext *m_context;
	
	GLShader m_prog;
	GLMatrix m_modelMatrix;
	GLMatrix m_viewMatrix;
	
	long mouse_x, mouse_y;

DECLARE_EVENT_TABLE()
};

int GLView::TIMER1_ID(12);

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
	EVT_SIZE(GLView::resized)
	EVT_KEY_DOWN(GLView::keyPressed)
	EVT_MOTION(GLView::mouseMoved)
	EVT_LEFT_DOWN(GLView::mousePress)
	EVT_PAINT(GLView::render)
	EVT_TIMER(TIMER1_ID, GLView::timer1Elapsed)
END_EVENT_TABLE()

GLView::GLView(wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_timer1(new wxTimer(this, TIMER1_ID))
	, m_context(nullptr)
{
}

GLView::~GLView()
{
	if (m_context)
		delete m_context;
}

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(130,
in vec3 vertex;
in vec3 normal;
in vec2 omega;
uniform mat4 pmat;
uniform mat4 vmat;
uniform mat4 mmat;
uniform mat3 nmmat;
uniform mat3 nvmat;
uniform vec3 light;
out vec3 p;
out vec2 w;
out vec3 n;
out vec3 l;
void main(void)
{
	//mat3 nmmat = transpose(inverse(mat3(mmat)));
	//mat3 nvmat = transpose(inverse(mat3(vmat)));
	
	w = omega;
	n = nvmat * nmmat * normal;
	l = nvmat * light;

	vec4 v = vmat * mmat * vec4(vertex, 1);	
	p = v.xyz;
	
	gl_Position = pmat * v;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(130,
uniform vec2 delta;
in vec3 p;
in vec2 w;
in vec3 n;
in vec3 l;
out vec4 c_out;
void main (void)
{
	vec2 m = mod(w, 2.0 * delta);
	m = step(delta, m);
	const vec4 c1 = vec4(0.5,1,0, 0); 
	const vec4 c2 = vec4(1,0.2,0.2, 0); 
	bool k = bool(m.x) ^^ bool(m.y);
	c_out = k ? c1 : c2;
	
	vec3 N = n;
	if (dot(p, n) > 0.0)
		N = -n;
	
	float fd = clamp(dot(l, N), 0, 1);
	float fs = pow(clamp(dot(reflect(normalize(p), n), l), 0, 1), 4.0);
	c_out *= mix(0.2, 1, fd);
	c_out += vec4(1, 1, 1, 1) * fs;
}
); 

#define VERTEX 0
#define NORMAL 1
#define OMEGA 2

void GLView::initGL()
{
	//cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	
	glewInit(); // glew charge les extensions disponibles
	
	cout << "opengl : " << glGetString(GL_VERSION) << endl;
	cout << "glsl : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	//cout << "exts : " << glGetString(GL_EXTENSIONS) << endl;

	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.bindAttributeLocation("vertex", VERTEX);
	m_prog.bindAttributeLocation("normal", NORMAL);
	m_prog.bindAttributeLocation("omega", OMEGA);
	m_prog.link();
	
	m_prog.bind();
	m_prog.setUniformValue("light", 1/sqrt(2), 1/sqrt(2), 0);
	m_prog.release();

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glHint(GL_FRAGMENT_SHADER_DERIVATIVE_HINT, GL_NICEST);

	m_viewMatrix = GLMatrix::translate(0, 0, -2);

	wxSizeEvent evt;
	resized(evt);
	m_timer1->Start(40);
	//cout << "ok" << endl;
}
	
void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);
		
		m_prog.bind(); // OpenGL 3
		m_prog.setUniformMatrix("pmat", GLMatrix::perspective(40.0, float(w) / float(h?h:1), 0.01, 1000));
		m_prog.release();
	}
	event.Skip();
}

GLVec alpha(float s, float t)
{
	return GLVec(sin(s)*cos(t), sin(s)*sin(t), cos(s));
//	return GLVec(t*cos(s), t*sin(s), 1-t*t);
	return GLVec(s*sin(t), t*0.02, s*cos(t));
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;
	//cout << "drawing" << endl;
	SetCurrent(*m_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_prog.bind(); // active le GLShader
	m_prog.setUniformMatrix("mmat", m_modelMatrix);
	m_prog.setUniformMatrix("vmat", m_viewMatrix); 
	m_prog.setUniformMatrix3("nmmat", m_modelMatrix.inverted(), true);
	m_prog.setUniformMatrix3("nvmat", m_viewMatrix.inverted(), true);
	
	glEnable(GL_DEPTH_TEST);

	//glPolygonMode(GL_BACK, GL_LINE);
	//glEnable(GL_CULL_FACE);

	float s_min = 0;
	float s_max = 0.1;
	float t_min = 0;
	float t_max = 5*M_PI;
	int s_n = 15;
	int t_n = 70;
	
//	s_min = 0.0; s_max = 2*M_PI; s_n = 10;
//	t_min = 0.0; t_max = 1.0;	 t_n = 10;
	
	s_min = 0; s_max = M_PI; s_n = 10;
	t_min = 0.0; t_max = 1.5*M_PI;	 t_n = 10;
	
	float s_d = (s_max - s_min) / float(s_n);
	float t_d = (t_max - t_min) / float(t_n);
	m_prog.setUniformValue("delta", s_d, t_d);

	for (int i = 0; i < s_n; ++i) {
		for (int j = 0; j < t_n; ++j) {
			float s = s_min + s_d * i;
			float t = t_min + t_d * j;
			GLVec A = alpha(s, t);
			GLVec B = alpha(s+s_d, t);
			GLVec C = alpha(s+s_d, t+t_d);
			GLVec D = alpha(s, t+t_d);
			GLVec NA = GLVec::cross(B-A, D-A).normalized();
			GLVec NB = GLVec::cross(C-B, A-B).normalized();
			GLVec NC = GLVec::cross(D-C, B-C).normalized();
			GLVec ND = GLVec::cross(A-D, C-D).normalized();
			
			glBegin(GL_QUADS);
			glVertexAttrib2f(OMEGA, s, t);
			glVertexAttrib3f(NORMAL, NA.x, NA.y, NA.z);
			glVertexAttrib3f(VERTEX, A.x, A.y, A.z);

			glVertexAttrib2f(OMEGA, s+s_d, t);
			glVertexAttrib3f(NORMAL, NB.x, NB.y, NB.z);
			glVertexAttrib3f(VERTEX, B.x, B.y, B.z);

			glVertexAttrib2f(OMEGA, s+s_d, t+t_d);
			glVertexAttrib3f(NORMAL, NC.x, NC.y, NC.z);
			glVertexAttrib3f(VERTEX, C.x, C.y, C.z);

			glVertexAttrib2f(OMEGA, s, t+t_d);
			glVertexAttrib3f(NORMAL, ND.x, ND.y, ND.z);
			glVertexAttrib3f(VERTEX, D.x, D.y, D.z);
			glEnd();
		}
	}

	m_prog.release();

	glFlush();
	SwapBuffers();
}

// EVENTS
void GLView::keyPressed(wxKeyEvent& event)
{
	// Multiplie la matrice à GAUCHE
	switch(event.GetKeyCode()) {
	case 'Q':
		m_viewMatrix = GLMatrix::rotate(1, 0, 0, -1) * m_viewMatrix;
		Refresh(false);	
		break;
	case 'E':
		m_viewMatrix = GLMatrix::rotate(-1, 0, 0, -1) * m_viewMatrix;
		Refresh(false);
		break;
	case 'W':
		m_viewMatrix = GLMatrix::translate(0, 0, 0.1) * m_viewMatrix;
		Refresh(false);
		break;
	case 'S':
		m_viewMatrix = GLMatrix::translate(0, 0, -0.1) * m_viewMatrix;
		Refresh(false);
		break;
	case 'A':
		m_viewMatrix = GLMatrix::translate(0.1, 0, 0) * m_viewMatrix;
		Refresh(false);
		break;
	case 'D':
		m_viewMatrix = GLMatrix::translate(-0.1, 0, 0) * m_viewMatrix;
		Refresh(false);
		break;
	case 'R':
		m_viewMatrix = GLMatrix::translate(0, -0.1, 0) * m_viewMatrix;
		Refresh(false);
		break;
	case 'F':
		m_viewMatrix = GLMatrix::translate(0, 0.1, 0) * m_viewMatrix;
		Refresh(false);
		break;
	}
	event.Skip();
}

void GLView::mousePress(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	mouse_x = x;
	mouse_y = y;
	event.Skip();
}

void GLView::mouseMoved(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	float dx = x - mouse_x;
	float dy = y - mouse_y;
	float d = sqrt(dx*dx + dy*dy);
	mouse_x = x;
	mouse_y = y;
	
	if (event.LeftIsDown()) {
		m_viewMatrix = GLMatrix::rotate(0.1 * d, dy, dx, 0) * m_viewMatrix;
		Refresh(false);
	}
	event.Skip();
}

void GLView::timer1Elapsed(wxTimerEvent& event)
{	
	float dt = 1.0 / 60.0 * event.GetInterval();
	m_modelMatrix *= GLMatrix::rotate(dt, 0, 1, 0);
	Refresh(false);
}







class MyApp: public wxApp
{
private:
	bool OnInit() override;

	wxFrame *_frame;
	GLView *_glPane;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	
	//cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("main"), wxPoint(50,50), wxSize(500,500));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(_frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	_frame->Show();

	//cout << "ok" << endl;

	_glPane->initGL();
	
	return true;
}

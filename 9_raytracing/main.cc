#include <GL/glew.h> // pour les extentions
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/mstream.h>
#include <iostream>

#include "glmatrix.hh"
#include "glshader.hh"
#include "glvec.hh"
#include "gltexture.hh"
#include "glsl.hh"
#include "cubemap.hh"

using namespace std;

class GLView : public wxGLCanvas
{
public:
	GLView(wxFrame* parent, int* args);
	~GLView();

	void loadTexture(int faceId, unsigned int size, const unsigned char *data);

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);

	void keyPressed(wxKeyEvent& event);
	void mousePress(wxMouseEvent& event);
	void mouseMoved(wxMouseEvent& event);
	void timer1Elapsed(wxTimerEvent& event);

private:

	GLuint textures[6];

	static int TIMER1_ID;
	wxTimer* m_timer1;

	wxGLContext *m_context;
	
	GLShader m_prog;
	GLMatrix m_modelMatrix;
	GLMatrix m_viewMatrix;
	
	long mouse_x, mouse_y;

DECLARE_EVENT_TABLE()
};

int GLView::TIMER1_ID(12);

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
	EVT_SIZE(GLView::resized)
	EVT_KEY_DOWN(GLView::keyPressed)
	EVT_MOTION(GLView::mouseMoved)
	EVT_LEFT_DOWN(GLView::mousePress)
	EVT_PAINT(GLView::render)
	EVT_TIMER(TIMER1_ID, GLView::timer1Elapsed)
END_EVENT_TABLE()

GLView::GLView(wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_timer1(new wxTimer(this, TIMER1_ID))
	, m_context(nullptr)
{
}

GLView::~GLView()
{
	glDeleteTextures(6, textures);

	if (m_context)
		delete m_context;
}

void GLView::loadTexture(int faceId, unsigned int size, const unsigned char *data) 
{
	wxMemoryInputStream input(data, size);
	glBindTexture(GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceId, textures[faceId]);
	GLTexture::loadTexture(input, GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceId, GLTexture::LinearFiltering);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);
}

#define VERTEX 0

void GLView::initGL()
{
	//cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	
	glewInit(); // glew charge les extensions disponibles
	
	cout << "opengl : " << glGetString(GL_VERSION) << endl;
	cout << "glsl : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	//cout << "exts : " << glGetString(GL_EXTENSIONS) << endl;

	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.bindAttributeLocation("vertex", VERTEX);
	m_prog.link();
	
	m_prog.bind();
	m_prog.setUniformValue("anglevalue", 1.428f);
	m_prog.setUniformValue("light", -0.3/1.0863, 1.0/1.0863, 0.3/1.0863);
	m_prog.setUniformValue("light", 0.5773502691896258, 0.5773502691896258, 0.5773502691896258);

	// transparente
	m_prog.setUniformValue("spheres[0].center", 0.0, 0.0, -3.0);
	m_prog.setUniformValue("spheres[0].radius", 1.0f);
	m_prog.setUniformValue("spheres[0].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("spheres[0].mat.ambiant", 0.0, 0.0, 0.0);
	m_prog.setUniformValue("spheres[0].mat.diffuse", 0.0, 0.0, 0.0);
	m_prog.setUniformValue("spheres[0].mat.eta", 1.3f);

	// rouge
	m_prog.setUniformValue("spheres[1].center", -1.5, 2.0, -5.5);
	m_prog.setUniformValue("spheres[1].radius", 1.0f);
	m_prog.setUniformValue("spheres[1].mat.phong_factor", 1.f);
	m_prog.setUniformValue("spheres[1].mat.ambiant", 0.3, 0.0, 0.0);
	m_prog.setUniformValue("spheres[1].mat.diffuse", 0.9, 0.0, 0.0);
	m_prog.setUniformValue("spheres[1].mat.eta", 0.0f);

	// transparente
	m_prog.setUniformValue("spheres[2].center", 5.0, 0.0, -10);
	m_prog.setUniformValue("spheres[2].radius", 2.5f);
	m_prog.setUniformValue("spheres[2].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("spheres[2].mat.ambiant", 0.0, 0.0, 0.0);
	m_prog.setUniformValue("spheres[2].mat.diffuse", 0.0, 0.0, 0.0);
	m_prog.setUniformValue("spheres[2].mat.eta", 1.2f);

	// transparente
	m_prog.setUniformValue("spheres[3].center", -2.5, -1.5, -20);
	m_prog.setUniformValue("spheres[3].radius", 2.0f);
	m_prog.setUniformValue("spheres[3].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("spheres[3].mat.ambiant", 0.0, 0.0, 0.0);
	m_prog.setUniformValue("spheres[3].mat.diffuse", 0.0, 0.0, 0.0);
	m_prog.setUniformValue("spheres[3].mat.eta", 1.5f);

	m_prog.setUniformValue("planes[0].point", 0.0, -2.5, -8.0);
	m_prog.setUniformValue("planes[0].normal", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[0].width", 0.0, 0.0, -5.0);
	m_prog.setUniformValue("planes[0].height", 0.0, 5.0, 0.0);
	m_prog.setUniformValue("planes[0].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("planes[0].mat.ambiant", 0.5, 0.0, 0.0);
	m_prog.setUniformValue("planes[0].mat.diffuse", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[0].mat.eta", 1.3f);

	m_prog.setUniformValue("planes[1].point", -5.0, -2.5, -8.0);
	m_prog.setUniformValue("planes[1].normal", 0.0, 0.0, 1.0);
	m_prog.setUniformValue("planes[1].width", 5.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[1].height", 0.0, 5.0, 0.0);
	m_prog.setUniformValue("planes[1].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("planes[1].mat.ambiant", 0.5, 0.0, 0.0);
	m_prog.setUniformValue("planes[1].mat.diffuse", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[1].mat.eta", 1.3f);

	m_prog.setUniformValue("planes[2].point", -5.0, 2.5, -8.0);
	m_prog.setUniformValue("planes[2].normal", 0.0, 1.0, 0.0);
	m_prog.setUniformValue("planes[2].width", 5.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[2].height", 0.0, 0.0, -5.0);
	m_prog.setUniformValue("planes[2].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("planes[2].mat.ambiant", 0.5, 0.0, 0.0);
	m_prog.setUniformValue("planes[2].mat.diffuse", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[2].mat.eta", 1.3f);

	m_prog.setUniformValue("planes[3].point", -5.0, -2.5, -8.0);
	m_prog.setUniformValue("planes[3].normal", 0.0, -1.0, 0.0);
	m_prog.setUniformValue("planes[3].width", 5.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[3].height", 0.0, 0.0, -5.0);
	m_prog.setUniformValue("planes[3].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("planes[3].mat.ambiant", 0.5, 0.0, 0.0);
	m_prog.setUniformValue("planes[3].mat.diffuse", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[3].mat.eta", 1.3f);

	m_prog.setUniformValue("planes[4].point", -5.0, -2.5, -8.0);
	m_prog.setUniformValue("planes[4].normal", -1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[4].width", 0.0, 0.0, -5.0);
	m_prog.setUniformValue("planes[4].height", 0.0, 5.0, 0.0);
	m_prog.setUniformValue("planes[4].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("planes[4].mat.ambiant", 0.5, 0.0, 0.0);
	m_prog.setUniformValue("planes[4].mat.diffuse", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[4].mat.eta", 1.3f);

	m_prog.setUniformValue("planes[5].point", -5.0, -2.5, -13.0);
	m_prog.setUniformValue("planes[5].normal", 0.0, 0.0, -1.0);
	m_prog.setUniformValue("planes[5].width", 5.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[5].height", 0.0, 5.0, 0.0);
	m_prog.setUniformValue("planes[5].mat.phong_factor", 0.0f);
	m_prog.setUniformValue("planes[5].mat.ambiant", 0.5, 0.0, 0.0);
	m_prog.setUniformValue("planes[5].mat.diffuse", 1.0, 0.0, 0.0);
	m_prog.setUniformValue("planes[5].mat.eta", 1.3f);

	m_prog.setUniformValue("cubemap", 0);
	m_prog.setUniformValue("tex", 1);
	m_prog.release();

	glGenTextures(6, textures);
	loadTexture(0, posx_jpg_size, posx_jpg);
	loadTexture(1, negx_jpg_size, negx_jpg);
	loadTexture(2, posy_jpg_size, posy_jpg);
	loadTexture(3, negy_jpg_size, negy_jpg);
	loadTexture(4, posz_jpg_size, posz_jpg);
	loadTexture(5, negz_jpg_size, negz_jpg);

	glClearColor(0.0, 0.0, 0.0, 1.0);

	wxSizeEvent evt;
	resized(evt);
	m_timer1->Start(40);
	//cout << "ok" << endl;
}

void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);
		
		m_prog.bind();
		m_prog.setUniformValue("aspect", GLfloat(w) / GLfloat(h?h:1));
		m_prog.release();
	}
	event.Skip();
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;
	SetCurrent(*m_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);

	m_prog.bind();
	m_prog.setUniformMatrix("view", m_viewMatrix);
	m_prog.setUniformMatrix3("nview", m_viewMatrix.inverted(), true);
	
	glBegin(GL_QUADS);
	glVertexAttrib2f(VERTEX, -1.0, -1.0);
	glVertexAttrib2f(VERTEX, +1.0, -1.0);
	glVertexAttrib2f(VERTEX, +1.0, +1.0);
	glVertexAttrib2f(VERTEX, -1.0, +1.0);
	glEnd();
	m_prog.release();

	glFlush();
	SwapBuffers();
}

// EVENTS
void GLView::keyPressed(wxKeyEvent& event)
{
	// Multiplie la matrice à GAUCHE
	switch(event.GetKeyCode()) {
	case 'Q':
		m_viewMatrix = m_viewMatrix * GLMatrix::rotate(-1, 0, 0, -1);
		Refresh(false);	
		break;
	case 'E':
		m_viewMatrix = m_viewMatrix * GLMatrix::rotate(1, 0, 0, -1);
		Refresh(false);
		break;
	case 'W':
		m_viewMatrix = m_viewMatrix * GLMatrix::translate(0, 0, -0.1);
		Refresh(false);
		break;
	case 'S':
		m_viewMatrix = m_viewMatrix * GLMatrix::translate(0, 0, 0.1);
		Refresh(false);
		break;
	case 'A':
		m_viewMatrix = m_viewMatrix * GLMatrix::translate(-0.1, 0, 0);
		Refresh(false);
		break;
	case 'D':
		m_viewMatrix = m_viewMatrix * GLMatrix::translate(+0.1, 0, 0);
		Refresh(false);
		break;
	case 'R':
		m_viewMatrix = m_viewMatrix * GLMatrix::translate(0, +0.1, 0);
		Refresh(false);
		break;
	case 'F':
		m_viewMatrix = m_viewMatrix * GLMatrix::translate(0, -0.1, 0);
		Refresh(false);
		break;
	}
	event.Skip();
}

void GLView::mousePress(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	mouse_x = x;
	mouse_y = y;
	event.Skip();
}

void GLView::mouseMoved(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	float dx = x - mouse_x;
	float dy = y - mouse_y;
	float d = sqrt(dx*dx + dy*dy);
	mouse_x = x;
	mouse_y = y;
	
	if (event.LeftIsDown()) {
		m_viewMatrix = m_viewMatrix * GLMatrix::rotate(-0.1 * d, dy, dx, 0);
		Refresh(false);
	}
	event.Skip();
}

void GLView::timer1Elapsed(wxTimerEvent& event)
{	
	float dt = 1.0 / 60.0 * event.GetInterval();
	m_modelMatrix *= GLMatrix::rotate(dt, 0, 1, 0);
	Refresh(false);
}







class MyApp: public wxApp
{
private:
	bool OnInit() override;

	wxFrame *_frame;
	GLView *_glPane;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	
	//cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("main"), wxPoint(50,50), wxSize(400,400));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(_frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	_frame->ShowFullScreen(true);

	//cout << "ok" << endl;

	_glPane->initGL();
	
	return true;
}
